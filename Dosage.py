class Dosage:

    class WeeklyAmount:
        LOW: float = 0
        MEDIUM: float = 21
        HIGH: float = 49

    @staticmethod
    def getLevel(amount: float) -> float:
        if amount < Dosage.WeeklyAmount.MEDIUM:
            return Dosage.WeeklyAmount.LOW
        elif amount < Dosage.WeeklyAmount.HIGH:
            return Dosage.WeeklyAmount.MEDIUM
        else:
            return Dosage.WeeklyAmount.HIGH
