from typing import *
import csv
from Patient import Patient

class FileIO(object):



    def __init__(self):
        # data paths
        self.DATA_PATH: str = "./data/warfarin.csv"

        self.DELIMITER = ","
        # self.QUOTE = '"'
        # self.QUOTING_STYLE = csv.QUOTE_ALL
        #
        # self.TINY_TOY_DATA_PATH = "./data/tinyToyInputs.txt"
        #
        # self.WORDS_TO_INDICES_FILE = "./data/size_fifty_indices_map.pkl"

    def readInPatients(self) -> List[Patient]:
        patients: List[Patient] = []
        with open(self.DATA_PATH) as dataFile:
            reader: Iterator = csv.reader(dataFile, delimiter=self.DELIMITER)
            for row in reader:
                newPatient: Optional[Patient] = Patient.fromCsvRow(row)
                if newPatient is not None:
                    patients.append(newPatient)
        return patients
    #
    #
    # def readInData(self, type: str) -> List[List[str]]:
    #     sentences = []
    #     with open(self.DATA_MAP[type]) as dataFile:
    #         reader = csv.reader(dataFile, delimiter=self.DELIMITER)
    #         for row in reader:
    #             sentences.append(row[len(row) - 1].split(" "))
    #     return sentences
    #
    # def saveResults(self, timestamp: str, iteration: str, trainScore: float, devScore: float) -> None:
    #     fileName = self.RESULTS_DIR + self.RESULTS_PREFIX + "_" + timestamp + ".txt"
    #     with open(fileName, mode="w") as file:
    #         file.write("Iteration: " + str(iteration))
    #         file.write("Train: " + str(trainScore))
    #         file.write("Dev: " + str(devScore))
    #
    # def getModelSavePath(self, runMode: str, timestamp: str):
    #     return self.RESULTS_DIR + self.RESULTS_PREFIX + "_" + runMode[2:] + "_" + timestamp + ".bin"
    #
    # def loadWordEmbedings(self) -> torch.Tensor:
    #     return torch.load(self.WORD_EMBEDDINGS_FILE)
    #
    # def saveWordEmbeddings(self, tensor: torch.Tensor) -> None:
    #     torch.save(tensor, self.WORD_EMBEDDINGS_FILE)
    #
    # # def saveInputs(self, npData: np.ndarray, type: str) -> None:
    # #     np.savetxt(self.INPUTS_FILE_MAP[type], npData, fmt='%d')
    #
    # # def loadInputs(self, type) -> np.ndarray:
    # #     return np.loadtxt(self.INPUTS_FILE_MAP[type], dtype=int)
    #
    # def getInputsPath(self, type: str, device: str) -> str:
    #     return self.GPU_INPUTS_FILE_MAP[type] if device == CLArgs.Device.CUDA else self.CPU_INPUTS_FILE_MAP[type]
    #
    # def loadWordsToIndicesMap(self) -> Dict[str, int]:
    #     return pickle.load(open(self.WORDS_TO_INDICES_FILE, 'rb'))
    #
    # def generateBatches(self, reader: csv.reader, batchSize: int) -> List[List[str]]:
    #
    #     batch = []
    #     for i, row in enumerate(reader):
    #         if len(batch) == batchSize:
    #             yield batch
    #             del batch[:]
    #         else:
    #             # Logging.log(type(row))
    #             batch.append(row)
    #     if len(batch) > 0:
    #         # Logging.log(type(batch[0]))
    #         # Logging.log(type(batch[0][0]))
    #         yield batch
    #
    # # for chunk in gen_chunks(reader):
    # #     print chunk # process chunk
    # #
    # # test gen_chunk on some dummy sequence:
    # # for chunk in gen_chunks(range(10), chunksize=3):
    # #     print chunk # process chunk
    #
    # @staticmethod
    # def generateDataDirectory(dataDir: str, fileName: str, suffix: str) -> str:
    #     return dataDir + fileName + suffix
    #
