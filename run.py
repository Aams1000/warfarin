from Patient import Patient
from fileio import FileIO
from evaluations import Evaluator
from models.predictingAlgorithm import PredictingAlgorithm
from models.LassoBandit import LassoBandit
from models.fixedDose import FixedDose
from models.clinicalDosing import ClinicalDosing
from models.linucb import LinearUCB
from models.featureVector import FeatureVectorConverter
import random

from typing import *

def readInData() -> List[Patient]:
    return FileIO().readInPatients()


def evaluateAllAlgorithms(patients: List[Patient]) -> None:

    # print("Evaluating FixedDose:")
    # fixedDose:
    # accuracy, message = evaluateAlgorithm(patients, FixedDose())
    # print(message)
    # print(linearUCB.getRegretAnalysis())
    # evaluateAlgorithm(patients, FixedDose())
    # evaluateAlgorithm(patients, LinearUCB(FeatureVectorConverter.NUM_FEATURES, len(patients)))
    evaluateAlgorithm(patients, LassoBandit(FeatureVectorConverter.NUM_FEATURES))

    # print("Evaluating ClinicalDosing:")
    # accuracy, message = evaluateAlgorithm(patients, ClinicalDosing())
    # print(message)

    # print("Evaluating LinUCB:")
    # random.shuffle(patients)
    # linearUCB: PredictingAlgorithm = LinearUCB(FeatureVectorConverter.NUM_FEATURES, len(patients))
    # accuracy, message = evaluateAlgorithm(patients, linearUCB)
    # print(message)
    # print(linearUCB.getRegretAnalysis())
    # random.shuffle(patients)
    # lasso: PredictingAlgorithm = LassoBandit(FeatureVectorConverter.NUM_FEATURES)
    # accuracy, message = evaluateAlgorithm(patients, lasso)
    # print(message)
    # print(lasso.getRegretAnalysis())


def evaluateAlgorithm(patients: List[Patient], algorithm: PredictingAlgorithm) -> None:
    print("Evaluating " + algorithm.getName())
    random.shuffle(patients)
    accuracy, message = executeAlgorithm(patients, algorithm)
    print(message)
    print(algorithm.getRegretAnalysis())


def executeAlgorithm(patients: List[Patient], algorithm: PredictingAlgorithm) -> Tuple[float, str]:
    evaluator: Evaluator = Evaluator(algorithm)
    for patient in patients:
        evaluator.evaluatePatient(patient)
    return evaluator.evaluatePerformance()


def main():
    patients: List[Patient] = readInData()
    # print("Total patients to test: " + str(len(patients)))
    evaluateAllAlgorithms(patients)



if __name__ == "__main__":
    main()

