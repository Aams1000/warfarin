from __future__ import annotations
from typing import *
from patientAttribute import Attribute
from patientAttributes import PatientAttributes

class Patient(object):

    # AGE_MAP = {"20 - 29": 2,
    #            "30 - 39": 3,
    #            "40 - 49": 4,
    #            "50 - 59": 5,
    #            "60 - 69": 6,
    #            "70 - 79": 7,
    #            "80 - 89": 8,
    #            "90+": 9}
    #
    # class Race:
    #     WHITE = Attribute("white", 0)
    #     ASIAN = Attribute("asian", 1)
    #     BLACK = Attribute("black or african american", 2)
    #     UNKNOWN = Attribute("unknown", 3)
    #
    # RACE_MAP = {"white" Patient.Race.WHITE,
    #             "asian" Patient.Race.ASIAN,
    #             "black or african american" Patient.Race.BLACK,
    #             "unknown" Patient.Race.UNKNOWN}
    #
    #
    #
    #
    # class Gender:
    #     MALE = Attribute("male", 0)
    #     FEMALE = Attribute("female", 1)


    def __init__(self, gender: Attribute,
                 age: int,
                 race: Attribute,
                 weight: float,
                 height: float,
                 medications: Set[str],
                 amiodaroneStatus: int,
                 enzymeInducerStatus: int,
                 correctDose: float,
                 VKORC1_9923231: str,
                 VKORC1_2884737: str,
                 cyp29: float,
                 VKORC1_9934438: float,
                 VKORC1_1542: float,
                 VKORC1_3730: float,
                 VKORC1_2255: float,
                 VKORC1_4451: float,
                 currentSmoker: float):
        self.gender: Attribute = gender
        self.age: int = age
        self.race: Attribute = race
        self.weight: float = weight
        self.height: float = height
        self.medications: Set[str] = medications
        self.amiodaroneStatus: int = amiodaroneStatus
        self.enzymeInducerStatus: int = enzymeInducerStatus
        self.correctDose: float = correctDose
        self.VKORC1_9923231: str = VKORC1_9923231
        self.VKORC1_2884737: str = VKORC1_2884737
        self.cyp29:float = cyp29
        self.VKORC1_9934438: float = VKORC1_9934438
        self.VKORC1_1542: float = VKORC1_1542
        self.VKORC1_3730: float = VKORC1_3730
        self.VKORC1_2255: float = VKORC1_2255
        self.VKORC1_4451: float = VKORC1_4451
        self.currentSmoker: float = currentSmoker

    def getGender(self) -> Attribute:
        return self.gender
    def getAge(self) -> int:
        return self.age
    def getRace(self) -> Attribute:
        return self.race
    def getWeight(self) -> float:
        return self.weight
    def getHeight(self) -> float:
        return self.height
    def getMedications(self) -> Set[str]:
        return self.medications
    def getAmiodaroneStatus(self) -> int:
        return self.amiodaroneStatus
    def getEnzymeInducerStatus(self) -> int:
        return self.enzymeInducerStatus
    def getRawDose(self) -> float:
        return self.correctDose
    def getVKORC1_9923231(self) -> str:
        return self.VKORC1_9923231
    def getVKORC1_2884737(self) -> str:
        return self.VKORC1_2884737
    def getCyp29(self) -> float:
        return self.cyp29
    def getVKORC1_9934438(self) -> float:
        return self.VKORC1_9934438
    def getVKORC1_1542(self) -> float:
        return self.VKORC1_1542
    def getVKORC1_3730(self) -> float:
        return self.VKORC1_3730
    def getVKORC1_2255(self) -> float:
        return self.VKORC1_2255
    def getVKORC1_4451(self) -> float:
        return self.VKORC1_4451
    def getCurrentSmoker(self) -> float:
        return self.currentSmoker
    #         @staticmethod
    #     def getGender(csvRow: List[str]) -> Attribute:
    #         genderString: str = csvRow[PatientAttributes.Gender].lower()
    #         return PatientAttributes.GENDER_MAP.get(genderString, None)
    #
    #     @staticmethod
    #     def getAge(csvRow: List[str]) -> int:
    #         ageString: str = csvRow[PatientAttributes.Age].lower()
    #         return PatientAttributes.AGE_MAP.get(ageString, -1)
    #
    #     @staticmethod
    #     def getRace(csvRow: List[str]) -> Attribute:
    #         raceString: str = csvRow[PatientAttributes.Race].lower()
    #         return PatientAttributes.RACE_MAP.get(raceString, None)
    #
    #     @staticmethod
    #     def getWeight(csvRow: List[str]) -> float:
    #         try:
    #             return float(csvRow[PatientAttributes.Weight_kg])
    #         except Exception as e:
    #             return -1
    #
    #     @staticmethod
    #     def getHeight(csvRow: List[str]) -> float:
    #         try:
    #             return float(csvRow[PatientAttributes.Height_cm])
    #         except Exception as e:
    #             return -1
    #
    #     @staticmethod
    #     def getMedications(csvRow: List[str]) -> Set[str]:
    #         try:
    #             return set(csvRow[PatientAttributes.Medications].split(";"))
    #         except Exception as e:
    #             return set()
    #
    #     @staticmethod
    #     def getAmiodaroneStatus(csvRow: List[str]) -> int:
    #         try:
    #             return int(csvRow[PatientAttributes.Amiodarone_Cordarone])
    #         except Exception as e:
    #             return -1
    #
    #     @staticmethod
    #     def getEnzymeInducerStatus(csvRow: List[str]) -> int:
    # #         carbamazepine, phenytoin, rifampin, or
    # # rifampicin
    #         try:
    #             carbamazepineStatus: int = int(csvRow[PatientAttributes.Carbamazepine_Tegretol])
    #             phenytoinStatus: int = int(csvRow[PatientAttributes.Phenytoin_Dilantin])
    #             rifampinStatus: int = int(csvRow[PatientAttributes.Rifampin_or_Rifampicin])
    #             return 1 if carbamazepineStatus + phenytoinStatus + rifampinStatus > 0 else 0
    #         except Exception as e:
    #             return -1

    @staticmethod
    def fromCsvRow(csvRow: List[str]) -> Optional[Patient]:
        gender: Attribute = PatientAttributes.getGender(csvRow)
        age: int = PatientAttributes.getAge(csvRow)
        race: Attribute = PatientAttributes.getRace(csvRow)
        weight: float = PatientAttributes.getWeight(csvRow)
        height: float = PatientAttributes.getHeight(csvRow)
        medications: Set[str] = PatientAttributes.getMedications(csvRow)
        amiodaroneStatus: int = PatientAttributes.getAmiodaroneStatus(csvRow)
        enzymeInducerStatus: int = PatientAttributes.getEnzymeInducerStatus(csvRow)
        correctDose: float = PatientAttributes.getCorrectDose(csvRow)
        VKORC1_9923231: str = PatientAttributes.getVKORC1_9923231(csvRow)
        VKORC1_2884737: str = PatientAttributes.getVKORC1_2884737(csvRow)
        cyp29: float = PatientAttributes.getCYP2C9(csvRow)
        VKORC1_9934438: float = PatientAttributes.getVKORC1_9934438(csvRow)
        VKORC1_1542: float = PatientAttributes.getVKORC1_1542(csvRow)
        VKORC1_3730: float = PatientAttributes.getVKORC1_3730(csvRow)
        VKORC1_2255: float = PatientAttributes.getVKORC1_2255(csvRow)
        VKORC1_4451: float = PatientAttributes.getVKORC1_4451(csvRow)
        currentSmoker: float = PatientAttributes.getCurrentSmoker(csvRow)

        if gender is None or race is None or age < 0 or weight < 0 or height < 0 or amiodaroneStatus < 0 or enzymeInducerStatus < 0 or correctDose < 0 or VKORC1_2884737 is None or VKORC1_9923231 is None or cyp29 < 0 or VKORC1_9934438 < 0 or VKORC1_1542 < 0 or VKORC1_3730 < 0 or VKORC1_2255 < 0 or VKORC1_4451 < 0 or currentSmoker < 0:
                # print("gender: " + str(gender))
                # print("age: " + str(age))
                # print("race: " + str(race))
                # print("weight: " + str(weight))
                # print("height: " + str(height))
                # print("medications: " + str(medications))
                # print("amiodaroneStatus: " + str(amiodaroneStatus))
                # print("enzymeInducerStatus: " + str(enzymeInducerStatus))
                # print("correctDose: " + str(correctDose))
                return None
        else:
            return Patient(gender, age, race, weight, height, medications, amiodaroneStatus, enzymeInducerStatus, correctDose, VKORC1_9923231, VKORC1_2884737, cyp29, VKORC1_9934438, VKORC1_1542, VKORC1_3730, VKORC1_2255, VKORC1_4451, currentSmoker)









