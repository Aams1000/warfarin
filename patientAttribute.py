class Attribute(object):

    def __init__(self, csvKey: str, value: float):
        self.csvKey: str = csvKey
        self.value: float = value

    def getCsvKey(self) -> str:
        return self.csvKey

    def getValue(self) -> float:
        return self.value
