from patientAttribute import Attribute
from csvPatient import CSVPatient
from typing import *


# self.age: int =
# self.weight: float =
# self.height: float =
# self.race: int =
# self.medications: List[str] =
class PatientAttributes:

    AGE_MAP = {"20 - 29": 2,
               "30 - 39": 3,
               "40 - 49": 4,
               "50 - 59": 5,
               "60 - 69": 6,
               "70 - 79": 7,
               "80 - 89": 8,
               "90+": 9}

    class Race:
        WHITE = Attribute("white", 0)
        ASIAN = Attribute("asian", 1)
        BLACK = Attribute("black or african american", 2)
        UNKNOWN = Attribute("unknown", 3)

    class Gender:
        MALE = Attribute("male", 0)
        FEMALE = Attribute("female", 1)

    RACE_MAP = {"white": Race.WHITE,
                "asian": Race.ASIAN,
                "black or african american": Race.BLACK,
                "unknown": Race.UNKNOWN}

    GENDER_MAP = {"male": Gender.MALE,
                  "female": Gender.FEMALE}

    class VKORC1_9923231:
        AA: str = "A/A"
        AG: str = "A/G"
        GG: str = "G/G"
        UN: str = "NA"

    VKORC1_9923231_SET: Set[str] = {VKORC1_9923231.AA, VKORC1_9923231.AG, VKORC1_9923231.GG, VKORC1_9923231.UN}

    class VKORC1_2884737:
        GG: str = "G/G"
        GT: str = "G/T"
        TT: str = "T/T"
        UN: str = "NA"

    VKORC1_9934438_MAP: Dict[str, float] = {"C/C": 0,
                                            "C/T": 1,
                                            "NA": 2,
                                            "T/T": 3}

    VKORC1_1542_MAP: Dict[str, float] = {"C/C": 0,
                                         "C/G": 1,
                                         "NA": 2,
                                         "G/G": 3}

    VKORC1_3730_MAP: Dict[str, float] = {"A/A": 0,
                                         "A/G": 1,
                                         "NA": 2,
                                         "G/G": 3}

    VKORC1_2255_MAP: Dict[str, float] = {"C/C": 0,
                                         "C/T": 1,
                                         "NA": 2,
                                         "T/T": 3}

    VKORC1_4451_MAP: Dict[str, float] = {"A/A": 0,
                                         "A/C": 1,
                                         "NA": 2,
                                         "C/C": 3}

    SMOKER_MAP: Dict[str, float] = {"0": 0,
                                    "1": 1,
                                    "NA": 2}

    VKORC1_2884737_SET: Set[str] = {VKORC1_2884737.GG, VKORC1_2884737.GT, VKORC1_2884737.TT, VKORC1_2884737.UN}

    CYP2C9_MAP: Dict[str, float] = {"*1/*1": 0,
                                  "NA": 1,
                                  "*1/*2": 2,
                                  "*1/*3": 3,
                                  "*2/*2": 4,
                                  "*2/*3": 5,
                                  "*3/*3": 6}


    @staticmethod
    def getGender(csvRow: List[str]) -> Attribute:
        genderString: str = csvRow[CSVPatient.Gender].lower()
        return PatientAttributes.GENDER_MAP.get(genderString, None)

    @staticmethod
    def getAge(csvRow: List[str]) -> int:
        ageString: str = csvRow[CSVPatient.Age].lower()
        return PatientAttributes.AGE_MAP.get(ageString, -1)

    @staticmethod
    def getRace(csvRow: List[str]) -> Attribute:
        raceString: str = csvRow[CSVPatient.Race].lower()
        return PatientAttributes.RACE_MAP.get(raceString, None)

    @staticmethod
    def getWeight(csvRow: List[str]) -> float:
        try:
            return float(csvRow[CSVPatient.Weight_kg])
        except Exception as e:
            return -1

    @staticmethod
    def getHeight(csvRow: List[str]) -> float:
        try:
            return float(csvRow[CSVPatient.Height_cm])
        except Exception as e:
            # print(csvRow[CSVPatient.Height_cm])
            return -1

    @staticmethod
    def getMedications(csvRow: List[str]) -> Set[str]:
        try:
            medications: Set[str] = set(csvRow[CSVPatient.Medications].split(";"))
            isEmpty: bool = "NA" in medications and len(medications) == 1
            return medications if not isEmpty else set()
        except Exception as e:
            return set()

    @staticmethod
    def getAmiodaroneStatus(csvRow: List[str]) -> int:
        try:
            return int(csvRow[CSVPatient.Amiodarone_Cordarone])
        except Exception as e:
            return 0

    @staticmethod
    def getCorrectDose(csvRow: List[str]) -> float:
        try:
            return float(csvRow[CSVPatient.Therapeutic_Dose_of_Warfarin])
        except Exception as e:
            return -1

    @staticmethod
    def getEnzymeInducerStatus(csvRow: List[str]) -> int:
#         carbamazepine, phenytoin, rifampin, or
# rifampicin
        carbamazepineStatus: int = 0
        phenytoinStatus: int = 0
        rifampinStatus: int = 0
        try:
            carbamazepineStatus: int = int(csvRow[CSVPatient.Carbamazepine_Tegretol])
        except Exception as e:
            carbamazepineStatus = 0
        try:
            phenytoinStatus: int = int(csvRow[CSVPatient.Phenytoin_Dilantin])
        except Exception as e:
            phenytoinStatus = 0
        try:
            rifampinStatus: int = int(csvRow[CSVPatient.Rifampin_or_Rifampicin])
        except Exception as e:
            rifampinStatus = 0
        return 1 if carbamazepineStatus + phenytoinStatus + rifampinStatus > 0 else 0

    @staticmethod
    def getVKORC1_9923231(csvRow: List[str]) -> str:
        genotype: str = csvRow[CSVPatient.VKORC1_genotype__1639_G_A_3673_chr1631015190_rs9923231_C_T]
        return genotype if genotype in PatientAttributes.VKORC1_9923231_SET else None


    @staticmethod
    def getVKORC1_2884737(csvRow: List[str]) -> str:
        genotype: str = csvRow[CSVPatient.VKORC1_genotype_497T_G_5808_chr1631013055_rs2884737_A_C]
        return genotype if genotype in PatientAttributes.VKORC1_2884737_SET else None


    @staticmethod
    def getCYP2C9(csvRow: List[str]) ->float :
        return PatientAttributes.CYP2C9_MAP.get(csvRow[CSVPatient.CYP2C9_consensus], -1)

    @staticmethod
    def getVKORC1_9934438(csvRow: List[str]) -> float:
        return PatientAttributes.VKORC1_9934438_MAP.get(csvRow[CSVPatient.VKORC1_genotype_1173_C_T6484_chr1631012379_rs9934438_A_G], -1)

    @staticmethod
    def getVKORC1_1542(csvRow: List[str]) -> float:
        return PatientAttributes.VKORC1_1542_MAP.get(csvRow[CSVPatient.VKORC1_1542_consensus], -1)

    @staticmethod
    def getVKORC1_3730(csvRow: List[str]) -> float:
        return PatientAttributes.VKORC1_3730_MAP.get(csvRow[CSVPatient.VKORC1_3730_consensus], -1)

    @staticmethod
    def getVKORC1_2255(csvRow: List[str]) -> float:
        return PatientAttributes.VKORC1_2255_MAP.get(csvRow[CSVPatient.VKORC1_2255_consensus], -1)

    @staticmethod
    def getVKORC1_4451(csvRow: List[str]) -> float:
        return PatientAttributes.VKORC1_4451_MAP.get(csvRow[CSVPatient.VKORC1__4451_consensus], -1)

    @staticmethod
    def getCurrentSmoker(csvRow: List[str]) -> float:
        return PatientAttributes.SMOKER_MAP.get(csvRow[CSVPatient.Current_Smoker], -1)


