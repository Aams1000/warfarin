import numpy as np
from Dosage import Dosage
from typing import *
from Patient import Patient
from models.predictingAlgorithm import PredictingAlgorithm
from models.featureVector import FeatureVectorConverter


class LinearUCB(PredictingAlgorithm):

    # ACTIONS: List[float] = [0.0, 0.5, 1.0]

    def __init__(self, numFeatures: int, numTrials: int):
        super(LinearUCB, self).__init__()
        self.numFeatures: int = numFeatures
        self.totalRegret: float = 0.0
        # self.A: np.ndarray = np.eye(numFeatures, dtype=np.float)
        # self.b: np.ndarray = np.zeros(shape=(numFeatures, 1))
        self.possibleActions: Dict[float, List[np.ndarray, np.ndarray]] = {Dosage.WeeklyAmount.LOW: [np.eye(numFeatures, dtype=np.float), np.zeros(shape=(numFeatures, 1))],
                                                                            Dosage.WeeklyAmount.MEDIUM: [np.eye(numFeatures, dtype=np.float), np.zeros(shape=(numFeatures, 1))],
                                                                            Dosage.WeeklyAmount.HIGH: [np.eye(numFeatures, dtype=np.float), np.zeros(shape=(numFeatures, 1))]}
        # self.zeroFeatureVector: np.ndarray = np.zeros(shape=(numFeatures, 1))
        # self.theta: np.ndarray = None

        self.epsilon: float = 0.0000001
        self.DELTA: float = 0.25
        self.NUM_ACTIONS: float = 3.0
        # self.numTrials: int = numTrials
        self.iteration: int = 0
        self.alpha: float = self.calculateAlpha(numTrials, self.NUM_ACTIONS, self.DELTA)
        # self.alpha: float = 0.5

    def calculateDose(self, patient: Patient) -> float:
        self.iteration += 1
        featureVector: np.ndarray = FeatureVectorConverter.convertPatientToVector(patient)
        # print(rawFeatures)
        # norm: float = np.linalg.norm(rawFeatures)
        # if norm > 1 + self.epsilon:
        #     raise ValueError("Feature vector has norm " + str(norm) + " greater than one.\n" + str(rawFeatures))
        # theta: np.ndarray = self.computeTheta(self.A, self.b)

        highestUCB: float = float("-inf")
        bestAction: float = float("-inf")
        # bestFeatureVector: np.ndarray = None
        for action, aAndB in self.possibleActions.items():
            A: np.array = aAndB[0]
            b: np.array = aAndB[1]
            # bNorm: float = np.linalg.norm(b)
            # b = b if bNorm == 0 else b / bNorm
            # print("Highest value: " + str(highestUCB))
            # print("Curr action: " + str(action))

            # featureVector: np.ndarray = self.computeFeatureVector(rawFeatures, action)
            theta: np.ndarray = self.computeTheta(A, b)
            # print("theta norm: " + str(np.linalg.norm(theta)))
            # featureVector: np.ndarray = rawFeatures
            # print(featureVector)
            # alpha: float = 1 / self.calculateAlpha(self.iteration, self.NUM_ACTIONS, self.DELTA)
            ucb: float = self.computeUCB(theta, featureVector, A, self.alpha)
            # print("UCB: " + str(ucb))
            # print(theta)
            if ucb > highestUCB:
                highestUCB = ucb
                bestAction = action
                # bestFeatureVector = featureVector
        isCorrect: bool = bestAction == Dosage.getLevel(patient.getRawDose())
        reward: float = 0.0 if isCorrect else -1.0
        self.totalRegret += reward

        # print("Correct dose: " + str(Dosage.getLevel(patient.getCorrectDose())))
        print("Predicted dose: " + str(bestAction))
        print("Result: " + str(isCorrect))
        # print("Reward: " + str(reward))

        # for action, aAndB in self.possibleActions.items():
        #     aAndB[0] += np.outer(featureVector, featureVector)
        #     if action != patient.getCorrectDose():
        #         aAndB[1] -= featureVector
        # outerProduct: np.ndarray = np.outer(featureVector, featureVector)
        # print("Outer product shape: " + str(outerProduct.shape))
        self.possibleActions[bestAction][0] = self.possibleActions[bestAction][0] + np.outer(featureVector, featureVector)
        # if isCorrect:
        #     print(self.possibleActions[bestAction][1])
            # print(reward * featureVector)
        self.possibleActions[bestAction][1] = self.possibleActions[bestAction][1] + reward * featureVector
        # if isCorrect:
        #     print(self.possibleActions[bestAction][1])
        # print(self.possibleActions)

        print(self.iteration, np.abs(self.totalRegret))
        return bestAction

    @staticmethod
    def computeUCB(theta: np.ndarray, featureVector: np.ndarray, A: np.ndarray, alpha: float) -> float:
        firstPart: float = np.dot(theta.T, featureVector)
        secondPart: float = alpha * np.sqrt(np.dot(np.dot(featureVector.T, np.linalg.inv(A)), featureVector))
        # print(firstPart + secondPart)
        # print(alpha)
        # print(firstPart)
        # print(secondPart)
        # print("Second part shape: " + str(secondPart.shape))
        return firstPart + secondPart

    @staticmethod
    def computeTheta(A: np.ndarray, b: np.ndarray) -> np.ndarray:
        # print("A shape: " + str(A.shape))
        # print("b shape: " + str(b.shape))
        theta: np.ndarray = np.dot(np.linalg.inv(A), b)
        thetaNorm: float = np.linalg.norm(theta)
        # return theta / thetaNorm if thetaNorm > 0 else theta
        return theta

    @staticmethod
    def calculateAlpha(T: int, K: float, delta: float) -> float:
        return np.sqrt(0.5 * np.log((2 * T * K) / delta))

    def getRegretAnalysis(self) -> str:
        return "Experienced total regret of " + str(np.abs(self.totalRegret)) + " over " + str(self.iteration) + " patients"

    def getName(self) -> str:
        return "LinUCB"
    # def mapPatientToVector(self, patient: Patient) -> np.ndarray:
    #     rawFeatures: np.ndarray = FeatureVectorConverter.convertPatientToVector(patient)
    #     print(rawFeatures)
    #     return 0.0

    # def computeFeatureVector(self, patientVector: np.ndarray, action: float) -> np.ndarray:
    #     # print("Patient vector shape: " + str(patientVector.shape))
    #     # print("Zero vector shape: " + str(self.zeroFeatureVector.shape))
    #     if action == Dosage.WeeklyAmount.LOW:
    #         return np.concatenate([patientVector, self.zeroFeatureVector, self.zeroFeatureVector], axis=0)
    #     elif action == Dosage.WeeklyAmount.MEDIUM:
    #         return np.concatenate([self.zeroFeatureVector, patientVector, self.zeroFeatureVector], axis=0)
    #     elif action == Dosage.WeeklyAmount.HIGH:
    #         return np.concatenate([self.zeroFeatureVector, self.zeroFeatureVector, patientVector], axis=0)


