from Patient import Patient
from Dosage import Dosage
from models.predictingAlgorithm import PredictingAlgorithm
import numpy as np

# This shouldn't be an object, but I'm making it one so I can pass it to something else
class FixedDose(PredictingAlgorithm):

    def __init__(self):
        super(FixedDose, self).__init__()
        self.WEEKLY_DOSAGE = Dosage.WeeklyAmount.MEDIUM
        self.iteration: int = 0
        self.totalRegret: float = 0.0

    def calculateDose(self, patient: Patient) -> float:
        self.iteration += 1
        isCorrect: bool = self.WEEKLY_DOSAGE == Dosage.getLevel(patient.getRawDose())
        reward: float = 0.0 if isCorrect else -1.0
        self.totalRegret += reward
        print(self.iteration, np.abs(self.totalRegret))

        return self.WEEKLY_DOSAGE

    def getRegretAnalysis(self) -> str:
        return "Experienced total regret of " + str(np.abs(self.totalRegret)) + " over " + str(self.iteration) + " patients"

    def getName(self) -> str:
        return "FixedDose"