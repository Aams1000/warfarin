from models.predictingAlgorithm import PredictingAlgorithm
from Patient import Patient
from patientAttributes import PatientAttributes
import numpy as np

class ClinicalDosing(PredictingAlgorithm):

    def __init__(self):
        super(PredictingAlgorithm, self).__init__()
        self.INTERCEPT: float = 4.0376
        self.COEFFICIENT_AGE: float = -0.2546
        self.COEFFICIENT_HEIGHT: float = 0.0118
        self.COEFFICIENT_WEIGHT: float = 0.0134
        self.COEFFICIENT_ENZYME: float = 1.2799
        self.COEFFICIENT_AMIODARONE: float = -0.5695

        self.COEFFICIENT_ASIAN: float = -0.6752
        self.COEFFICIENT_BLACK: float = 0.4060
        self.COEFFICIENT_MIXED_MISSING_RACE: float = 0.0443

    # 4.0376
    # -
    # 0.2546 x
    # Age in decades
    # +
    # 0.0118 x
    # Height in cm
    # +
    # 0.0134 x
    # Weight in kg
    # -
    # 0.6752 x
    # Asian race
    # +
    # 0.4060 x
    # Black or African American
    # +
    # 0.0443 x
    # Missing or Mixed race
    # +
    # 1.2799 x
    # Enzyme inducer status
    # -
    # 0.5695 x
    # Amiodarone status

    def calculateDose(self, patient: Patient) -> float:
        ageValue: float = patient.getAge() * self.COEFFICIENT_AGE
        heightValue: float = patient.getHeight() * self.COEFFICIENT_HEIGHT
        weightValue: float = patient.getWeight() * self.COEFFICIENT_WEIGHT
        enzymeValue: float = patient.getEnzymeInducerStatus() * self.COEFFICIENT_ENZYME
        amiodaroneValue: float = patient.getAmiodaroneStatus() * self.COEFFICIENT_AMIODARONE
        raceValue: float = self.calculateRaceComponent(patient.getRace().getValue())

        squareRootWeeklyDose: float = self.INTERCEPT + ageValue + heightValue + weightValue + enzymeValue + amiodaroneValue + raceValue
        return squareRootWeeklyDose ** 2

    def calculateRaceComponent(self, race: float) -> float:
        value: float = 0
        if race == PatientAttributes.Race.ASIAN.getValue():
            value = self.COEFFICIENT_ASIAN
        elif race == PatientAttributes.Race.BLACK.getValue():
            value = self.COEFFICIENT_BLACK
        elif race == PatientAttributes.Race.UNKNOWN.getValue():
            value = self.COEFFICIENT_MIXED_MISSING_RACE

        return value

