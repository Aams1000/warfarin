from sklearn.linear_model import Lasso
from models.predictingAlgorithm import PredictingAlgorithm
from typing import *
from Dosage import Dosage
import numpy as np
from Patient import Patient
from models.featureVector import FeatureVectorConverter


class LassoBandit(PredictingAlgorithm):

    def __init__(self, numFeatures: int):
        super(LassoBandit, self).__init__()
        # TODO - what should these parameters be?
        self.allLassosAlphaInit: float = 1
        self.forcedLassosAlpha: float = 1
        self.q: float = 20
        self.h: float = 2
        self.numActions: int = 3
        self.numFeatures: int = numFeatures
        self.totalRegret: float = 0.0
        self.totalTrueMiddleDoses: int = 0
        self.totalTrueLowDoses: int = 0
        self.totalTrueHighDoses: int = 0


        self.forcedTimestepsByArm: Dict[float, Set[int]] = {Dosage.WeeklyAmount.LOW: self.constructForcedTimestepSet(1, self.numActions, self.q),
                                                            Dosage.WeeklyAmount.MEDIUM: self.constructForcedTimestepSet(2, self.numActions, self.q),
                                                            Dosage.WeeklyAmount.HIGH: self.constructForcedTimestepSet(3, self.numActions, self.q)}
        self.allTimestepsByArm: Dict[float, Set[int]] = {Dosage.WeeklyAmount.LOW: set(),
                                                         Dosage.WeeklyAmount.MEDIUM: set(),
                                                         Dosage.WeeklyAmount.HIGH: set()}

        self.forcedLassosByArm: Dict[float, Lasso] = {Dosage.WeeklyAmount.LOW: Lasso(warm_start=True, alpha=self.forcedLassosAlpha),
                                                      Dosage.WeeklyAmount.MEDIUM: Lasso(warm_start=True, alpha=self.forcedLassosAlpha),
                                                      Dosage.WeeklyAmount.HIGH: Lasso(warm_start=True, alpha=self.forcedLassosAlpha)}

        self.allLassosByArm: Dict[float, Lasso] = {Dosage.WeeklyAmount.LOW: Lasso(warm_start=True, alpha=self.allLassosAlphaInit),
                                                   Dosage.WeeklyAmount.MEDIUM: Lasso(warm_start=True, alpha=self.allLassosAlphaInit),
                                                   Dosage.WeeklyAmount.HIGH: Lasso(warm_start=True, alpha=self.allLassosAlphaInit)}
        self.iteration: int = 0

        # Contains data, labels. stack on bottom each time we update
        self.forcedDataByArm: Dict[float, List[np.ndarray]] = {Dosage.WeeklyAmount.LOW: None,
                                                                                  Dosage.WeeklyAmount.MEDIUM: None,
                                                                                  Dosage.WeeklyAmount.HIGH: None}

        # Contains data, labels. stack on bottom each time we update
        self.allDataByArm: Dict[float, List[np.ndarray]] = {Dosage.WeeklyAmount.LOW: None,
                                                                                  Dosage.WeeklyAmount.MEDIUM: None,
                                                                                  Dosage.WeeklyAmount.HIGH: None}

    def calculateDose(self, patient: Patient):
        self.iteration += 1
        action: float = -1
        featureVector: np.ndarray = FeatureVectorConverter.convertPatientToVector(patient)
        forcedDosage: int = self.getForceDosage(self.iteration, self.forcedTimestepsByArm)
        # print(self.iteration, forcedDosage)
        if forcedDosage >= 0:
            action = forcedDosage
            # print("Forced action: " + str(action))
            reward: int = 0 if forcedDosage == Dosage.getLevel(patient.getRawDose()) else -1
            # print(action, Dosage.getLevel(patient.getRawDose()), reward)
            # print(featureVector.T.shape)
            # print(np.asarray(reward).shape)
            self.addDataToArm(self.forcedDataByArm, forcedDosage, featureVector.T, reward)
            self.forcedLassosByArm[forcedDosage] = self.fitNewLasso(self.forcedDataByArm[forcedDosage][0], self.forcedDataByArm[forcedDosage][1], self.forcedLassosAlpha)
            # self.forcedLassosByArm[forcedDosage].fit(featureVector.T, np.reshape(np.asarray(reward), newshape=(1, 1)))
        else:
            # FIXME - placeholder
            selectedActions: List[float] = self.getCandidateActionsFromForcedData(self.forcedLassosByArm, featureVector.T, self.h)
            action = self.getMaximizingAction(self.allLassosByArm, featureVector.T, selectedActions)
            print("Best candidate action: " + str(action))


        # self.allTimestepsByArm[action].add(self.iteration)
        reward: int = 0 if action == Dosage.getLevel(patient.getRawDose()) else -1
        self.totalRegret += reward
        self.addDataToArm(self.allDataByArm, action, featureVector.T, reward)
        newAlpha: float = self.calculateNewAllLassoAlpha(self.allLassosAlphaInit, self.iteration, self.numFeatures)
        self.refitArms(self.allLassosByArm, self.allDataByArm, newAlpha)


        if Dosage.getLevel(patient.getRawDose()) == Dosage.WeeklyAmount.MEDIUM:
            self.totalTrueMiddleDoses += 1
        elif Dosage.getLevel(patient.getRawDose()) == Dosage.WeeklyAmount.LOW:
            self.totalTrueLowDoses += 1
        elif Dosage.getLevel(patient.getRawDose()) == Dosage.WeeklyAmount.HIGH:
            self.totalTrueHighDoses += 1
        else:
            raise ValueError("Invalid dosage from data: " + str(Dosage.getLevel(patient.getRawDose())) + ", raw value: " + str(patient.getRawDose()))
        # for key, lasso in self.allLassosByArm.items():
        #     # print("Old alpha: " + str(lasso.alpha))
        #     lasso.set_params(alpha=newAlpha)
        #     # print("New alpha: " + str(lasso.alpha))

        # self.allLassosByArm[action].fit(featureVector.T, np.reshape(np.asarray(reward), newshape=(1, 1)))

        # print(self.iteration, np.abs(self.totalRegret))

        return action

    @staticmethod
    def fitNewLasso(inputs: np.ndarray, rewards: np.ndarray, newAlpha: float) -> Lasso:
        lasso: Lasso = Lasso(alpha=newAlpha)
        lasso.fit(inputs, rewards)
        return lasso

    @staticmethod
    def refitArms(allLassosByArm: Dict[float, Lasso], allDataByArm: Dict[float, List[np.ndarray]], newAlpha: float) -> None:
        for arm in allLassosByArm.keys():
            if allDataByArm[arm] is not None:
                inputsAndRewards: List[np.ndarray] = allDataByArm[arm]
                allLassosByArm[arm] = LassoBandit.fitNewLasso(inputsAndRewards[0], inputsAndRewards[1], newAlpha)

    @staticmethod
    def addDataToArm(dataByArm: Dict[float, List[np.ndarray]], action: float, featureVector: np.ndarray, rewardFlt: float) -> None:
        reward: np.ndarray = np.reshape(np.asarray(rewardFlt), newshape=(1, 1))
        if dataByArm[action] is None:
            inputsAndRewards: List[np.ndarray] = []
            inputsAndRewards.append(featureVector)
            inputsAndRewards.append(reward)
            dataByArm[action] = inputsAndRewards
        else:
            inputs: np.ndarray = dataByArm[action][0]
            labels: np.ndarray = dataByArm[action][1]
            dataByArm[action][0] = np.concatenate([inputs, featureVector], axis=0)
            dataByArm[action][1] = np.concatenate([labels, reward], axis=0)

        # print(dataByArm[action])

    @staticmethod
    def getMaximizingAction(allLassosByArm: Dict[float, Lasso], featureVector: np.ndarray, selectedActions: List[float]) -> float:
        bestAction: float = -1
        highestPrediction: float = float("-inf")
        for action in selectedActions:
            prediction: float = allLassosByArm[action].predict(featureVector)
            # print("Prediction for arm " + str(action) + ": " + str(prediction))
            if prediction > highestPrediction:
                # print("Updating prediction from " + str(highestPrediction) + " to " + str(prediction))
                highestPrediction = prediction
                bestAction = action
        return bestAction

        # Contains data, labels. stack on bottom each time we update
        # self.dataByArm: Dict[float, List[np.ndarray]] = {Dosage.WeeklyAmount.LOW, None,
        #                                            Dosage.WeeklyAmount.MEDIUM, None,
        #                                            Dosage.WeeklyAmount.HIGH, None}

    @staticmethod
    def getCandidateActionsFromForcedData(forcedLassosByArm: Dict[float, Lasso], featureVector: np.ndarray, h: float) -> List[float]:
        maxPrediction: float = -1
        predictionsByArm: Dict[float, float] = {}
        for arm, lasso in forcedLassosByArm.items():
            prediction = lasso.predict(featureVector)
            predictionsByArm[arm] = prediction
            if prediction > maxPrediction:
                maxPrediction = prediction

        selectedActions: List[float] = []
        for arm, prediction in predictionsByArm.items():
            # print(prediction)
            if prediction >= maxPrediction - h / 2.0:
                # print("Max prediction: " + str(maxPrediction) + " Including:" + str(prediction))
                selectedActions.append(arm)

        return selectedActions

    @staticmethod
    def getForceDosage(value: int, forcedTimestepsByArm: Dict[float, Set[int]]) -> float:
        forcedDosage: int = -1
        for dosage, forceSet in forcedTimestepsByArm.items():
            if value in forceSet:
                forcedDosage = dosage
        return forcedDosage

    @staticmethod
    def calculateNewAllLassoAlpha(initialAlpha: float, t: int, numFeatures: int):
        return initialAlpha * np.sqrt((np.log(t) + np.log(numFeatures)) / float(t))

    @staticmethod
    def updateAlpha(lasso: Lasso, newAlpha: float) -> None:
        lasso.set_params(alpha=newAlpha)

    @staticmethod
    def constructForcedTimestepSet(i: float, K: float, q: float) -> Set[int]:
        jRange: List[int] = [q * (i - 1) + l for l in range(1, int((q * i - (q * (i - 1) + 1)) + 1))]
        listForm: List[int] = [((2 ** n) - 1) * K * q + j for n in range(0, 14) for j in jRange]
        # hack to make sure each gets initialized before we call fit on them
        actionIndices: List[int] = [1, 2, 3]
        for index in actionIndices:
            if index in listForm:
                listForm.remove(index)

        listForm.append(i)
        # print(listForm)

        return set(listForm)

    def getRegretAnalysis(self) -> str:
        return "Experienced total regret of " + str(np.abs(self.totalRegret)) + " over " + str(self.iteration) + " patients. " + str(self.totalTrueLowDoses) + " patients had a true low dose, " + str(self.totalTrueMiddleDoses) + " patients had a true medium dose, " + str(self.totalTrueHighDoses) + " patients had a true high dose"
        # self.forcedTimestepsLow: Set[int] = set()
        # self.forcedTimestepsMed: Set[int] = set()
        # self.forcedTimestepsHigh: Set[int] = set()
        #
        # self.allTimestepsLow: Set[int] = set()
        # self.allTimestepsMed: Set[int] = set()
        # self.allTimstepsHigh: Set[int] set()

    def getName(self) -> str:
        return "LassoBandit"