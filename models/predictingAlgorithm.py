from Patient import Patient


class PredictingAlgorithm(object):

    def __init__(self):
        # nothing here
        x = 5

    def calculateDose(self, patient: Patient) -> float:
        raise NotImplementedError("This must be implemented by the implementing class.")

    def getRegretAnalysis(self) -> str:
        raise NotImplementedError("This must be implemented by the implementing class.")

    def getName(self) -> str:
        raise NotImplementedError("This must be implemented by the implementing class.")
