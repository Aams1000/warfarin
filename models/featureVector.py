from Patient import Patient
import numpy as np
from typing import *
from patientAttributes import PatientAttributes


class FeatureVectorConverter:

    NUM_FEATURES: int = 46

    INDEX_AGE: int = 0
    INDEX_GENDER: int = 1
    INDEX_WEIGHT: int = 2
    INDEX_HEIGHT: int = 3
    INDEX_AMIODARONE: int = 4
    INDEX_ENZYME: int = 5
    INDEX_RACE_WHITE: int = 6
    INDEX_RACE_ASIAN: int = 7
    INDEX_RACE_BLACK: int = 8
    INDEX_RACE_UNKNOWN: int = 9
    INDEX_rs9923231_AA: int = 10
    INDEX_rs9923231_AG: int = 11
    INDEX_rs9923231_GG: int = 12
    INDEX_rs9923231_UN: int = 13
    INDEX_rs2884737_GG: int = 14
    INDEX_rs2884737_GT: int = 15
    INDEX_rs2884737_TT: int = 16
    INDEX_rs2884737_UN: int = 17

    SCALING_FACTOR_AGE: float = 10.0
    SCALING_FACTOR_HEIGHT: float = 202.0
    SCALING_FACTOR_WEIGHT: float = 237.7


    @staticmethod
    def convertPatientToVector(patient: Patient) -> np.ndarray:
        vector: List[float] = []

        # vector[FeatureVectorConverter.INDEX_AGE] = patient.getAge() / FeatureVectorConverter.SCALING_FACTOR_AGE
        # vector[FeatureVectorConverter.INDEX_GENDER] = patient.getGender().getValue()
        # vector[FeatureVectorConverter.INDEX_WEIGHT] = patient.getWeight() / FeatureVectorConverter.SCALING_FACTOR_WEIGHT
        # vector[FeatureVectorConverter.INDEX_HEIGHT] = patient.getHeight() / FeatureVectorConverter.SCALING_FACTOR_HEIGHT
        # vector[FeatureVectorConverter.INDEX_AMIODARONE] = patient.getAmiodaroneStatus()
        # vector[FeatureVectorConverter.INDEX_ENZYME] = patient.getEnzymeInducerStatus()
        # vector[FeatureVectorConverter.INDEX_RACE_WHITE] = patient.getRace().getValue() == PatientAttributes.Race.WHITE.getValue()
        # vector[FeatureVectorConverter.INDEX_RACE_ASIAN] = patient.getRace().getValue() == PatientAttributes.Race.ASIAN.getValue()
        # vector[FeatureVectorConverter.INDEX_RACE_BLACK] = patient.getRace().getValue() == PatientAttributes.Race.BLACK.getValue()
        # vector[FeatureVectorConverter.INDEX_RACE_UNKNOWN] = patient.getRace().getValue() == PatientAttributes.Race.UNKNOWN.getValue()
        # vector[FeatureVectorConverter.INDEX_rs9923231_AA] = patient.getVKORC1_9923231() == PatientAttributes.VKORC1_9923231.AA
        # vector[FeatureVectorConverter.INDEX_rs9923231_AG] = patient.getVKORC1_9923231() == PatientAttributes.VKORC1_9923231.AG
        # vector[FeatureVectorConverter.INDEX_rs9923231_GG] = patient.getVKORC1_9923231() == PatientAttributes.VKORC1_9923231.GG
        # vector[FeatureVectorConverter.INDEX_rs9923231_UN] = patient.getVKORC1_9923231() == PatientAttributes.VKORC1_9923231.UN
        # vector[FeatureVectorConverter.INDEX_rs2884737_GG] = patient.getVKORC1_2884737() == PatientAttributes.VKORC1_2884737.GG
        # vector[FeatureVectorConverter.INDEX_rs2884737_GT] = patient.getVKORC1_2884737() == PatientAttributes.VKORC1_2884737.GT
        # vector[FeatureVectorConverter.INDEX_rs2884737_TT] = patient.getVKORC1_2884737() == PatientAttributes.VKORC1_2884737.TT
        # vector[FeatureVectorConverter.INDEX_rs2884737_UN] = patient.getVKORC1_2884737() == PatientAttributes.VKORC1_2884737.UN




        vector.append(patient.getAge() / FeatureVectorConverter.SCALING_FACTOR_AGE)
        vector.append(patient.getGender().getValue())
        vector.append(patient.getWeight() / FeatureVectorConverter.SCALING_FACTOR_WEIGHT)
        vector.append(patient.getHeight() / FeatureVectorConverter.SCALING_FACTOR_HEIGHT)
        vector.append(patient.getAmiodaroneStatus())
        vector.append(patient.getEnzymeInducerStatus())
        vector.append(patient.getRace().getValue() == PatientAttributes.Race.WHITE.getValue())
        vector.append(patient.getRace().getValue() == PatientAttributes.Race.ASIAN.getValue())
        vector.append(patient.getRace().getValue() == PatientAttributes.Race.BLACK.getValue())
        vector.append(patient.getRace().getValue() == PatientAttributes.Race.UNKNOWN.getValue())
        vector.append(patient.getVKORC1_9923231() == PatientAttributes.VKORC1_9923231.AA)
        vector.append(patient.getVKORC1_9923231() == PatientAttributes.VKORC1_9923231.AG)
        vector.append(patient.getVKORC1_9923231() == PatientAttributes.VKORC1_9923231.GG)
        vector.append(patient.getVKORC1_9923231() == PatientAttributes.VKORC1_9923231.UN)
        vector.append(patient.getVKORC1_2884737() == PatientAttributes.VKORC1_2884737.GG)
        vector.append(patient.getVKORC1_2884737() == PatientAttributes.VKORC1_2884737.GT)
        vector.append(patient.getVKORC1_2884737() == PatientAttributes.VKORC1_2884737.TT)
        vector.append(patient.getVKORC1_2884737() == PatientAttributes.VKORC1_2884737.UN)

        vector.append(patient.getCyp29() == 0)
        vector.append(patient.getCyp29() == 1)
        vector.append(patient.getCyp29() == 2)
        vector.append(patient.getCyp29() == 3)
        vector.append(patient.getCyp29() == 4)
        vector.append(patient.getCyp29() == 5)
        vector.append(patient.getCyp29() == 6)

        vector.append(patient.getVKORC1_9934438() == 0)
        vector.append(patient.getVKORC1_9934438() == 1)
        vector.append(patient.getVKORC1_9934438() == 2)
        vector.append(patient.getVKORC1_9934438() == 3)

        vector.append(patient.getVKORC1_1542() == 0)
        vector.append(patient.getVKORC1_1542() == 1)
        vector.append(patient.getVKORC1_1542() == 2)
        vector.append(patient.getVKORC1_1542() == 3)

        vector.append(patient.getVKORC1_3730() == 0)
        vector.append(patient.getVKORC1_3730() == 1)
        vector.append(patient.getVKORC1_3730() == 2)
        vector.append(patient.getVKORC1_3730() == 3)

        vector.append(patient.getVKORC1_2255() == 0)
        vector.append(patient.getVKORC1_2255() == 1)
        vector.append(patient.getVKORC1_2255() == 2)
        vector.append(patient.getVKORC1_2255() == 3)

        vector.append(patient.getVKORC1_4451() == 0)
        vector.append(patient.getVKORC1_4451() == 1)
        vector.append(patient.getVKORC1_4451() == 2)
        vector.append(patient.getVKORC1_4451() == 3)

        vector.append(patient.getCurrentSmoker())

        wrongShapeVector: np.ndarray = np.asarray(vector)
        npVector: np.ndarray = np.reshape(wrongShapeVector, newshape=(FeatureVectorConverter.NUM_FEATURES, 1))
        # return npVector / np.linalg.norm(npVector)
        return npVector











# from patientAttribute import Attribute
# from csvPatient import CSVPatient
# from typing import *
#
#
# # self.age: int =
# # self.weight: float =
# # self.height: float =
# # self.race: int =
# # self.medications: List[str] =
# class PatientAttributes:
#
#     AGE_MAP = {"20 - 29": 2,
#                "30 - 39": 3,
#                "40 - 49": 4,
#                "50 - 59": 5,
#                "60 - 69": 6,
#                "70 - 79": 7,
#                "80 - 89": 8,
#                "90+": 9}
#
#     class Race:
#         WHITE = Attribute("white", 0)
#         ASIAN = Attribute("asian", 1)
#         BLACK = Attribute("black or african american", 2)
#         UNKNOWN = Attribute("unknown", 3)
#
#     class Gender:
#         MALE = Attribute("male", 0)
#         FEMALE = Attribute("female", 1)
#
#     RACE_MAP = {"white": Race.WHITE,
#                 "asian": Race.ASIAN,
#                 "black or african american": Race.BLACK,
#                 "unknown": Race.UNKNOWN}
#
#     GENDER_MAP = {"male": Gender.MALE,
#                   "female": Gender.FEMALE}
#
#
#
#
#
#
#     @staticmethod
#     def getGender(csvRow: List[str]) -> Attribute:
#         genderString: str = csvRow[CSVPatient.Gender].lower()
#         return PatientAttributes.GENDER_MAP.get(genderString, None)
#
#     @staticmethod
#     def getAge(csvRow: List[str]) -> int:
#         ageString: str = csvRow[CSVPatient.Age].lower()
#         return PatientAttributes.AGE_MAP.get(ageString, -1)
#
#     @staticmethod
#     def getRace(csvRow: List[str]) -> Attribute:
#         raceString: str = csvRow[CSVPatient.Race].lower()
#         return PatientAttributes.RACE_MAP.get(raceString, None)
#
#     @staticmethod
#     def getWeight(csvRow: List[str]) -> float:
#         try:
#             return float(csvRow[CSVPatient.Weight_kg])
#         except Exception as e:
#             return -1
#
#     @staticmethod
#     def getHeight(csvRow: List[str]) -> float:
#         try:
#             return float(csvRow[CSVPatient.Height_cm])
#         except Exception as e:
#             # print(csvRow[CSVPatient.Height_cm])
#             return -1
#
#     @staticmethod
#     def getMedications(csvRow: List[str]) -> Set[str]:
#         try:
#             medications: Set[str] = set(csvRow[CSVPatient.Medications].split(";"))
#             isEmpty: bool = "NA" in medications and len(medications) == 1
#             return medications if not isEmpty else set()
#         except Exception as e:
#             return set()
#
#     @staticmethod
#     def getAmiodaroneStatus(csvRow: List[str]) -> int:
#         try:
#             return int(csvRow[CSVPatient.Amiodarone_Cordarone])
#         except Exception as e:
#             return 0
#
#     @staticmethod
#     def getCorrectDose(csvRow: List[str]) -> float:
#         try:
#             return float(csvRow[CSVPatient.Therapeutic_Dose_of_Warfarin])
#         except Exception as e:
#             return -1
#
#     @staticmethod
#     def getEnzymeInducerStatus(csvRow: List[str]) -> int:
# #         carbamazepine, phenytoin, rifampin, or
# # rifampicin
#         carbamazepineStatus: int = 0
#         phenytoinStatus: int = 0
#         rifampinStatus: int = 0
#         try:
#             carbamazepineStatus: int = int(csvRow[CSVPatient.Carbamazepine_Tegretol])
#         except Exception as e:
#             carbamazepineStatus = 0
#         try:
#             phenytoinStatus: int = int(csvRow[CSVPatient.Phenytoin_Dilantin])
#         except Exception as e:
#             phenytoinStatus = 0
#         try:
#             rifampinStatus: int = int(csvRow[CSVPatient.Rifampin_or_Rifampicin])
#         except Exception as e:
#             rifampinStatus = 0
#         return 1 if carbamazepineStatus + phenytoinStatus + rifampinStatus > 0 else 0