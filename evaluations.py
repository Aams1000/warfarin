from models.predictingAlgorithm import PredictingAlgorithm
from Patient import Patient
from Dosage import Dosage
from typing import *


class Evaluator(object):

    def __init__(self, predictor: PredictingAlgorithm):
        self.numCorrectDoses: int = 0
        self.numTotalPredictions: int = 0
        self.predictor: PredictingAlgorithm = predictor

    def evaluatePatient(self, patient: Patient) -> None:
        self.numTotalPredictions += 1

        rawPrediction: float = self.predictor.calculateDose(patient)
        rawActualValue: float = patient.getRawDose()
        predictionCategory: float = Dosage.getLevel(rawPrediction)
        actualCategory: float = Dosage.getLevel(rawActualValue)

        # print("New patient:")
        # print("Raw prediction: " + str(rawPrediction))
        # print("Raw actual value: " + str(rawActualValue))
        # print("Predicted category: " + str(predictionCategory))
        # print("Actual category: " + str(actualCategory))
        if actualCategory == predictionCategory:
            # print("Predicted correctly\n")
            self.numCorrectDoses += 1

    def evaluatePerformance(self) -> Tuple[float, str]:
        successRatio: float = self.getSuccessRatio()
        message: str = "Predicted " + str(self.numCorrectDoses) + " out of " + str(self.numTotalPredictions) + " patients for " + str(successRatio) + " accuracy"
        return successRatio, message

    def getSuccessRatio(self) -> float:
        return self.numCorrectDoses / self.numTotalPredictions



